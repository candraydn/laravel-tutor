@extends('layouts.master')
@section('title','Article')
@section('content')
<div class="row py-4">
    @foreach ($articles as $article)
    <div class="col-md-4">
    <div class="card mb-3">
       <div class="card-body">
       <a href="/article/{{ $article->slug }}">{{ $article->title }}</a>
       <div>{{ $article->created_at->diffForHumans() }}</div>
       </div>
    </div>
    </div>
    @endforeach
</div>
@endsection
