@extends('layouts.master')
@section('title','Home')
@section('content')
<h1>Create Article</h1>
<form class="py-3" enctype="multipart/form-data" action="{{ route('article.store') }}" method="post">
    @csrf
    <div class="form-group">
        <label for="title">Title</label>
        <input name="title" type="text" class="form-control @error('title') is-invalid @enderror" id="title"
            placeholder="Enter titles" value="{{  old('title') }}">
        {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
    </div>
    <div class="form-group">
        <label for="body">Content</label>
        <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="body" rows=10>{{  old('body') }}</textarea>
    {!! $errors->first('body','<span class="invalid-feedback">:message</span>') !!}
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
