@extends('layouts.master')
@section('title','Detail')
@section('content')
    <h1 class="py-4">{{ $articles->title }}</h1>
<p>{{ $articles->body }}</p>
@endsection