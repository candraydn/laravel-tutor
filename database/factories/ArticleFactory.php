<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\articles;
use Faker\Generator as Faker;

$factory->define(articles::class, function (Faker $faker) {
    return [
        'title'=>$faker->sentence,
        'slug'=>\Str::slug($faker->sentence).'-'.\Str::slug(random(10)),
        'body'=>$faker->paragraph(10),
    ];
});