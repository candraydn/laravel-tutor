<?php

namespace App\Http\Controllers;

use App\articles;
use Illuminate\Http\Request;
use App\postingan;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Postingan::get();
        return view('welcome', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $art = request()->validate([
            'title' => ['required','min:5'],
            'body' => ['required','min:3']
        ]);
        
        $art['slug'] = \Str::slug($request->title).'-'.\Str::random(7);

        Articles::create($art);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $articles = Articles::orderBy('created_at','DESC')->get();
        return view('article', compact('articles'));
    }

    public function detail($slug)
    {
        $articles = Articles::whereSlug($slug)->first();
        if(is_null($articles)){
            abort(404);
        }
        return view('detail', compact('articles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
